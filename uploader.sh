#!/usr/bin/env bash
set -euo pipefail
es_host="127.0.0.1"
settings_file="settings.json"

for file in db/*; do
    case  $2 in
            json)
                python index_leak.py -j -i $1 -e $es_host -f $file -s $settings_file
                ;;
             csv)
                 python index_leak.py -c -i $1 -e $es_host -f $file -s $settings_file
                 ;;
    esac
done
