#!/usr/bin/env python3

import json

with open("test.txt", "r") as input_file:
    with open("out.json", "w") as out_file:
        for line in input_file:
            try:
                leak_line = {}
                email, password = line.split(":")
                leak_line["email"] = email
                leak_line["password"] = password.rstrip()
                out_file.write(json.dumps(leak_line) + "\n")
            except IndexError as e:
                print(e)
            except ValueError as e:
                pass
            continue
